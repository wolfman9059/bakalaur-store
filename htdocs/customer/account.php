<?php 

session_start();

if(!isset($_SESSION['cus_email'])){
    
    echo "<script>window.open('../checkout.php','_self')</script>";
    
}else{

$active='Account';
    include("includes/header.php");
    include("includes/db.php");
    include("customer/functions/function.php");

?>
   <!DOCTYPE html>
   <html>
   <head>
     <title>Bakalaiur-Store</title>
   </head>
   <body>
<div id="content"><!-- #content Begin -->
       <div class="container"><!-- container Begin -->
           <div class="col-md-12"><!-- col-md-12 Begin -->
               
               <ul class="breadcrumb"><!-- breadcrumb Begin -->
                   <li>
                       <a href="../index.php">Home</a>
                   </li>
                   <li>
                       My Account
                   </li>
               </ul><!-- breadcrumb Finish -->
               
           </div><!-- col-md-12 Finish -->
           <div class="col-md-3">
            <?php 
              include("includes/user_side.php");
            ?>
           </div>

           <div class="col-md-9">
              <div class="box"><!-- box Begin -->
                   <?php
                   
                   if (isset($_GET['my_order'])){
                       include("my_order.php");
                   }
                   
                   ?>
                   
                   <?php
                   
                   if (isset($_GET['pay_offline'])){
                       include("pay_offline.php");
                   }
                   
                   ?>
                   
                   <?php
                   
                   if (isset($_GET['edit_profile'])){
                       include("edit_profile.php");
                   }
                   
                   ?>
                   
                   <?php
                   
                   if (isset($_GET['change_pass'])){
                       include("change_pssw.php");
                   }
                   
                   ?>
                   
                   <?php
                   
                   if (isset($_GET['delete_account'])){
                       include("delete_acc.php");
                   }
                   
                   ?>
                   
              </div><!-- box Finish -->     
           </div><!-- col-md-9 Finish -->
       </div>
</div>
   <?php 
    
    include("includes/footer.php");
    
  ?>
   </body>
   </html>
<?php } ?>

    
    
    
    
